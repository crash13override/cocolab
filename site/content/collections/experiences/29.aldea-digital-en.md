title: 'Aldea Digital '
description: >
  For more than 5 years, “Aldea digital” has been celebrated in Mexico City, one of the largest
  digital inclusion events in the world where connectivity and digital education are promoted. For
  this event, COCOLAB is commissioned every year to create a series of digital experiences that
  immerse the audience and involve them interactively.
credits: |
  <b>Client</b></br>
  Telmex
  
  <b>Collaboration</b></br>
  CIE-CREA
place: CDMX
date: 2017-03-23
hero_image: /assets/experiences/experiencias_aldea_1.jpg
images:
  - /assets/experiences/experiencias_aldea_2.jpg
  - /assets/experiences/experiencias_aldea_3.jpg
  - /assets/experiences/04-1490312360.jpg
  - /assets/experiences/08-1490312371.jpg
videos:
  - >
    http://24484eb6bce00bc5e663-0c5141547f217d9cd388b36c785a71b0.r16.cf2.rackcdn.com/cocoweb/aldea_long_video.mp4
featured: false
id: 44f93744-fa3c-477f-92b6-8454b14538d7
tags: [ ]
