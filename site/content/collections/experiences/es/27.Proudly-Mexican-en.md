title: Orgullosamente Mexicanos
description: >
  Como parte del programa de actividades de las Conmemoraciones del Bicentenario de la independencia
  de México en el 2010, el Gobierno Federal comisionó este proyecto a “Creatividad y
  Espectáculos” quienes, en colaboración con COCOLAB, pudieron conceptualizar una experiencia que
  consistía en una mega pantalla en la que se contó la historia de México por medio de diversos
  cortos animados en los que participaron estudios de Canadá y México.
credits: |
  
  <b>Cliente</b></br>
  Gobierno Federal Mexicano
  
  <b>Colaboración<b></b>
  CREA
id: 557f9455-3d83-429a-8baa-ad4e10503cc6
slug: Proudly-Mexican-es
