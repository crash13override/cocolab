description: |
  La exposición “On! Handcrafted Digital Playgrounds”, curada por el fundador y director de OFFF Barcelona, Héctor Ayuso y celebrada en el Contemporary Arts Center de Cincinnati en los Estados Unidos, presentó a las mentes más brillantes que exploran el diseño y tecnología hoy en día, tales como: Atelier, Brendan Dawes, Brosmind, Hotpixel (miembro de COCOLAB), James Paterson, Jon Burgerman, James Victore, Joshua Davis, Julien Vallée, Eve Duhamel, Multitouch Barcelona, Vasava, Patchworks y Sara Blake.
  
  Para esta exhibición la única regla era que todas las piezas fueran creadas de manera manual, para esta exhibición se creó un juguete interactivo inspirado en la naturaleza, el cual cobraba vida con tan solo acercarse y tocarlo.
credits: |
  <b>Cliente</b></br>
  Centro de Arte Contemporaneo de Cincinnati</br>
  Hector Ayuso
id: eb991c21-e476-4d0e-a5f7-0cd4b52a561d
slug: beats-of-hidden-cells-es
