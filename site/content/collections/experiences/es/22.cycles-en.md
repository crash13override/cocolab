description: |
  “Cycles” es una experiencia audiovisual de inmersión en una escultura a gran escala que visualiza el sonido y el movimiento de la luz mediante el uso de una serie de proyectores láser ubicados alrededor de una estructura de anillos circulares.
  
  Para “Cycles”, COCOLAB pidió a los músicos: Julian Placencia, Shiro Schwarz, Eduardo Jiménez, Tijs Ham, Eduardo Jiménez y Sebastian Frisch diseñar composiciones sonoras únicas que se traducen en luz y se propagan por el espacio. Esta instalación ha sido presentada en el Festival TAG CDMX así como en el TodaysART Japón.
credits: |
  <b>Cliente</b></br>
  TAG CDMX
  ARCA
place: CDMX-JAPÓN
id: 4e1e4228-2b16-4c58-b9cb-fc34269e19a6
slug: cycles-es
