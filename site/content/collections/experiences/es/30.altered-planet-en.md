title: Planeta Alterado
description: 'El pabellón "390 ppm: Planeta alterado. Cambios climáticos y México” buscó, a través de su museografía multimedia diseñada por COCOLAB, concientizar al público sobre las consecuencias de sus acciones en el cambio climático.'
credits: |
  
  <b>Cliente</b></br>
  Parque Bicentenario Silao, Guanajuato.
  
  <b>Colaboración</b></br>
  Museografía José Enrique Ortiz Lanz
id: 209c3dbd-998d-4bcc-a7b5-04a9b4f93d50
slug: altered-planet-es
