title: Fantasy in Chiapas
description: |
  The objective was to develop a nocturnal spectacle that detonated the stay of one more night in the city and that contributed to the affluence of local public to the cathedral.
  
  For this, a permanent multimedia show was made using videomapping, ambient sound and architectural lighting. This experience is a permanent installation that is projected at night.
credits: |
  
  <b>Client</b></br>
  Secretary of Tourism of the State of Chiapas.
place: CHIAPAS MX
date: 2012-03-08
hero_image: /assets/experiences/01-1490313373.jpg
images:
  - /assets/experiences/02-1490313380.jpg
  - /assets/experiences/03-1490313381.jpg
  - /assets/experiences/04-1490313382.jpg
  - /assets/experiences/05-1490313383.jpg
  - /assets/experiences/06-1490313384.jpg
videos: [ ]
featured: false
tags: [ ]
id: 1a60b5d2-4304-4db9-bfd8-43c1286dd190
