title: Process of Metals
description: >
  The Met-Mex Museum of Metals is an educational space focused on the history, processes and uses of
  metals. The main objective of this project was to find a simple way to show the refining process of
  silver, gold and zinc. For this, an intuitive reactive surface was created for the user, which shows
  the information of this process in a very visual and understandable way.
credits: |
  <b>Client</b></br>
  Met-Mex Peñoles
place: TORREON MX
date: 2012-08-15
hero_image: /assets/experiences/experiencas_metales_1.jpg
images:
  - /assets/experiences/experiencias_2.jpg
  - /assets/experiences/penoles2-2023-1490309852.jpg
videos: [ ]
featured: true
tags: [ ]
id: 3ef00b89-0787-4b8a-b97d-95f0ffe40887
