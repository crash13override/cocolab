title: Cycles
description: |
  "Cycles" is an immersive audiovisual experience in a large-scale sculpture that visualizes the sound and movement of light through the use of a series of laser projectors located around a structure of circular rings.
  
  For "Cycles", COCOLAB asked the musicians: Julian Placencia, Shiro Schwarz, Eduardo Jiménez, Tijs Ham, Eduardo Jiménez and Sebastian Frisch to design unique sound compositions that translate into light and spread through space. This installation has been presented at the TAG CDMX Festival as well as at the TodaysART Japan.
credits: |
  <b>Client</b></br>
  TAG CDMX</br>
  ARCA
place: CDMX-JAPAN
date: 2015-03-12
hero_image: /assets/experiences/experienca_cycles_3.jpg
images:
  - /assets/experiences/experienca_cycles_1.jpg
  - /assets/experiences/experienca_cycles_2.jpg
  - /assets/experiences/experienca_cycles_4.jpg
videos:
  - >
    http://24484eb6bce00bc5e663-0c5141547f217d9cd388b36c785a71b0.r16.cf2.rackcdn.com/cocoweb/cycles_long_video.mp4
featured: true
tags: [ ]
id: 4e1e4228-2b16-4c58-b9cb-fc34269e19a6
