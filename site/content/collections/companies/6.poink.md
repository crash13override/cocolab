title: Poink
description: |
  -Technical Infrastructure-</br>
  Design, acquisition, operation and maintenance of technology for the implementation of technological projects as well as the installation of a global infrastructure.
image:
  - /assets/companies/poink_empresa.png
logo:
  - /assets/companies/logo/poink.png
id: 026b1e76-c7ca-4dcf-bec6-c7b2c535c77b
