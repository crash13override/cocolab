---
title: About us
sub_content: >
  We aspire to provoke in people the curiosity to question their own limits by erasing the line
  between the daily and the extraordinary, making them participants in new paradigms of entertainment.
  To achieve this, Cocolab is made up of seven companies that form a collaboration network specialized
  in different multimedia technologies.
fieldset: about
mount: companies
template: about
id: bdd353c7-eee6-41be-835a-bdb077a0b65c
tags: [ ]
---
We believe that inspiration is the engine of any form of creation, so our purpose is to inspire people and inspire ourselves. Inspire and create is a virtuous cycle of which we want to be an active part.