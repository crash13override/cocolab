---
title: Quiénes somos
sub_content: >
  Aspiramos a provocar en la gente la curiosidad de cuestionar sus propios límites borrando la línea
  entre lo cotidiano y lo extraordinario, haciéndolos participantes en nuevos paradigmas de
  entretenimiento. Para lograr esto, Cocolab está formado por siete empresas que forman una red de
  colaboración especializada en diferentes tecnologías multimedia.
id: bdd353c7-eee6-41be-835a-bdb077a0b65c
slug: quienes-somos
---
Creemos que la inspiración es el motor de cualquier forma de creación, por lo que nuestro propósito es inspirar a las personas e inspirarnos. Inspirar y crear es un ciclo virtuoso del cual queremos ser una parte activa.
